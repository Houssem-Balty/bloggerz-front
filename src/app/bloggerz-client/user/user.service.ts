import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../reusable/app-settings';



@Injectable()
export class APIUserService {
    constructor(private httpClient: HttpClient) {

    }


    addUser(user) {
        this.httpClient.post( AppSettings.GLOBAL_API_ENPOINT + 'user/add', user).subscribe(result => {
            console.log(result);
        });
    }


    loginWithPassword(email, password) {

    }



    CheckFacebookUserExists(user) {

        return this.httpClient.post(AppSettings.GLOBAL_API_ENPOINT + 'user/check', user);
    }
}
