import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService, SocialUser, LoginProvider } from 'angular4-social-login';
import { OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Observable } from 'rxjs';
import { PermissionService } from '../permission/permission.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class LoginguardService implements CanActivate  {
  _loggedIn ;
  _user;

  constructor(private jwt: JwtHelperService,private permissionService: PermissionService ,private loginService: LoginService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      return true;

}


}
