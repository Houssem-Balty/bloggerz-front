import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import {Injectable} from '@angular/core';




@Injectable()
export class JwtWrapperService{

    constructor(private jwtHelper: JwtHelperService){

    }

    getToken(){
        return this.jwtHelper.tokenGetter();
    }


    // returns decoded token
    decodeToken(token: string) {
        return this.jwtHelper.decodeToken(token);
    }



    //returns expiration Date 
    getExpirationDate(){
        return this.jwtHelper.getTokenExpirationDate()
    }

}