import {Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {ReviewOffersService} from './review-offers.service';
import { NgbDatepicker, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbdDatepickerPopup } from '../reusable/inline-date-picker/datepicker-popup';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import { NgbDateISOParserFormatter } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-parser-formatter';
import { customNgBFormatter } from '../reusable/dateFormatter/date-formatter.component';
@Component({
    selector: '<add-review-offer></add-review-offer>',
    templateUrl: './review-offer-add.component.html'
})

export class ReviewOfferAddComponent{
    private title;
    private description;
    private type;
    private criteria;
    private bountytype;
    private bounty;
    private tags;
    private expirydate: NgbDateStruct;
    message;


    // review offer form group
    offer = new FormGroup({
        title : new FormControl(),
        description: new FormControl(),
        category: new FormControl(),
        type: new FormControl(),
        tags: new FormControl(),
        bounty: new FormControl(),
        bountytype: new FormControl(),
        criteria: new FormControl(),
        expirydate: new FormControl()



    })

    constructor(private reviewOffersService : ReviewOffersService){

    }






    getFormData() {
        const jobOffer = this.offer.value;

        // overrided ngBStruct formatter created in reusable components
        const formatter = new customNgBFormatter();


        jobOffer.expirydate = formatter.format(jobOffer.expirydate);
        this.reviewOffersService.postReviewOffer(jobOffer).subscribe(response => {
            console.log(response);

        }, error => {
            console.log(error);
        });



    }




}
