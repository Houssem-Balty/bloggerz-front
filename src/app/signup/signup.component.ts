import { Component } from '@angular/core';
import { PermissionService } from '../permission/permission.service';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { Input } from '@angular/core';
import { APIUserService } from '../bloggerz-client/user/user.service';
import { Router } from '@angular/router';
import { JwtWrapperService } from '../guards/jwt-wrapper.service';
import { Md5 } from 'ts-md5/dist/md5';
import { LoginService } from '../login/login.service';

@Component({
    selector: '<signup> </signup>',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})


export class SignupComponent {
    private facebookId;
    private email;
    private password;
    private passwordConfirmation;
    private displayName;
    private blogName;
    private blogCategory;
    private facebook_access_token;
    private blogLink;
    private businessName;
    private step;
    private type;
    private isBlogger: boolean;
    blogger_form = new FormGroup({
        blogCategory: new FormControl(),
        blogLink: new FormControl(),
        blogName: new FormControl(),
    });
    business_form = new FormGroup({
        websiteLink: new FormControl(),
        businessName: new FormControl(),
    });


    constructor(private router: Router,
        private permissionService: PermissionService,
        private apiUserService: APIUserService,
        private jwtWrapperService: JwtWrapperService,
        private loginService: LoginService) {
        this.step = 1;
        this.isBlogger = true;
    }

    signupByFacebook($event) {
        $event.preventDefault();
        console.log($event);
        if (localStorage.getItem('access_token') !== null) {
            console.log(localStorage.getItem('access_token'))
            this.router.navigate(['login']);
        } else {
            this.permissionService.getFb().getLoginStatus().then(response => {
                console.log(response);
                switch (response.status) {
                    case 'connected' :
                        this.facebookId = response.authResponse.userID;
                        this.facebook_access_token = response.authResponse.accessToken;
                        console.log(JSON.stringify({ 'id': response.authResponse.userID, 'type': 'facebook' }))
                        this.apiUserService.CheckFacebookUserExists(JSON.stringify({ 'id': response.authResponse.userID, 'type': 'facebook' })).subscribe(exists => {
                            console.log('exists', exists)
                            if (!exists) {
                                this.permissionService.getUserProfile(response.authResponse.accessToken).then(profile => {
                                    console.log('profile', profile.name);
                                    this.displayName = profile.name;
                                    this.email = profile.email;
                                    this.step = 2;

                                })
                            } else {
                                //this.router.navigate(['login']);
                                console.log('doesnt')

                            }
                        });
                        break;
                    case 'not_authorized':
                        this.permissionService.getUserPermissions('public_profile,email').then(authReponse => {
                            console.log(authReponse);
                        })
                        break;
                    case 'unknown':
                        console.log('shit');
                        break
                }

            });


        }






    }

    getUserInformation(form: NgForm) {
        console.log(form.value);
        this.password = form.value.password;
        this.passwordConfirmation = form.value.passwordConfirmation;
        this.email = form.value.email;
        this.displayName = form.value.name;
        this.step = 3;
        console.log('type', form.value.type)


        this.apiUserService.addUser(this.createUserObject());




    }

    createUserObject() {
        var user;
        user = {
            facebook_id: this.facebookId,
            password: Md5.hashStr(this.password),
            email: this.email,
            displayName: this.displayName,
            username: this.facebookId,
            enabled: true,
            profilePicLink: "test",
            access_token: this.facebook_access_token,
        }

        return user;
    }

    getBlogInformation() {
            this.blogCategory = this.blogger_form.value.blogCategory;
            this.blogName = this.blogger_form.value.blogName;
            this.blogLink = this.blogger_form.value.blogLink;
    }


}
