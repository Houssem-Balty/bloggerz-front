import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {Md5} from 'ts-md5/dist/md5';
import { JwtModule } from '@auth0/angular-jwt';
import { UserService } from './user/user.service';
import { PageviewService } from './page/pageview.service';
import { PageviewComponent } from './page/pageview.component';
import { SocialLoginModule, AuthServiceConfig } from 'angular4-social-login';
import { FacebookLoginProvider } from 'angular4-social-login';
import { LoginComponent } from './login/login.component';
import { RouterModule, provideRoutes, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { PermissionComponent } from './permission/permission.component';
import { FacebookModule } from 'ngx-facebook';
import { ChartsModule } from "ng2-charts";
import { PermissionService } from './permission/permission.service';
import { MaininsightComponent } from './page/main-insight/main-insight.component';
import { PageviewFilterComponent } from './page/pageview-filter/pageview-filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagedetailsComponent } from './page/details/pagedetails.component' ;
import { LandingpageComponent } from './landingpage/langingpage.component';
import { SignupComponent } from './signup/signup.component';
import { LoginguardService } from './guards/login-guard.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { JwtWrapperService } from './guards/jwt-wrapper.service';
import { LoginPageGuardService } from './guards/login-page-guard.service';
import { LoginService } from './login/login.service';
import { ChartpageComponent } from './page/details/chart-page/chartpage.component';
import {DatePipe} from '@angular/common';
import { NgbdDatepickerRange } from './reusable/daterangepicker/datepicker-range';
import { NgbdModalOptions } from './reusable/modal/modal-options';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker';
import {PostinsightComponent} from './page/details/post-insight/post-insight.component';
import { ReviewOffersComponent } from './review-offer/review-offers.component';
import {ReviewOfferAddComponent} from './review-offer/review-offer-add.component';
import { ReviewOffersService } from './review-offer/review-offers.service';
import {APIUserService} from './bloggerz-client/user/user.service';
import { NgbdDatepickerPopup } from './reusable/inline-date-picker/datepicker-popup';
import { AppSettings } from './reusable/app-settings';
// Material components 
import {MatChipsModule} from '@angular/material/chips';
import {MatSidenavModule} from '@angular/material/sidenav';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatSort, MatTableDataSource} from '@angular/material';

//TODO organize imports to regroup services, modules and third party libraries
let config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('1128001770660612')
  }
]);


const routes = [
  {
    path: 'details/:id',
    component: PagedetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'permissions',
    component: PermissionComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'app',
    component: PageviewComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent,
  
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'review-offers',
    component: ReviewOffersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'review-offer/add',
    component: ReviewOfferAddComponent
  },
  {
    path : '**',
    component: LandingpageComponent,
    canActivate: [LoginguardService],
  },
  {
    path: 'test',
    component: NgbdDatepickerPopup,
  }
]; 

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

export function provideConfig() {
  return config;
}


@NgModule({
  declarations: [
    ReviewOffersComponent,
    NavbarComponent,
    AppComponent,
    ChartpageComponent,
    PostinsightComponent,
    UserComponent,
    LoginComponent,
    PageviewComponent,
    LoginComponent,
    PermissionComponent,
    MaininsightComponent,
    PageviewFilterComponent,
    PagedetailsComponent,
    LandingpageComponent,
    SignupComponent,
    NgbdDatepickerRange,
    NgbdModalOptions,
    ReviewOfferAddComponent,
    NgbdDatepickerPopup,
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    SocialLoginModule.initialize(config),
    FacebookModule.forRoot(),
    HttpClientModule,
    MatChipsModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatSelectModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
      }
    }),
    RouterModule.forRoot(routes),
    FormsModule

  ],
  providers: [APIUserService, LoginguardService, AuthGuardService ,UserService,ReviewOffersService,HttpClient , PageviewService, PermissionService, JwtWrapperService,{
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },LoginPageGuardService ,LoginService, DatePipe],
  bootstrap: [AppComponent],
  exports: [NavbarComponent]
  
})
export class AppModule { 
  
}
