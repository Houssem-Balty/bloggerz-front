import { TestBed, async } from '@angular/core/testing';
import { LoginComponent } from '../login.component';

describe('LoginComponent', () => { 
    it('says hello', () => { 
      expect(LoginComponent) 
          .toEqual(1); 
    });
  });

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
    }).compileComponents();
  })); 

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  
});
