import { Component, OnInit } from '@angular/core';
import { AuthService, SocialUser } from 'angular4-social-login';
import { FacebookLoginProvider } from 'angular4-social-login';
import {Router} from '@angular/router' ;
import {LoginService} from './login.service';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { JwtWrapperService } from '../guards/jwt-wrapper.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import {MatChipsModule} from '@angular/material/chips';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private user ;
  private email ;
  private password;
  private errorMessage;

  loginForm = new FormGroup({
     email: new FormControl(),
     password: new FormControl(),
  })
    constructor(private jwt: JwtHelperService ,private loginService: LoginService, private router: Router) {


    }

    test() {
        return 1 ; 
    }

    signInWithFB($event): void {
        $event.preventDefault();
      if (!this.loginService.loggedIn) {
       this.loginService.signInWithFB()

      }
    }


    signInWithCredentials(){
        console.log('clicked');
        this.loginService.signInWithCredentials(this.loginForm.value).subscribe(token => {
            console.log(token);
            localStorage.setItem('access_token', token.token);
            //this.router.navigate(['app']);
        }, error => {
            if (error.status === 401){
                this.errorMessage = "Invalid credentials"
            }
        });

    }

    signOut(): void {
        this.loginService.signOut();
        this.user = null ;
        localStorage.removeItem('access_token')
    }

    ngOnInit(){

    }

}
