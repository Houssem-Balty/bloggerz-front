import { Component, OnInit, Injectable } from '@angular/core';
import { AuthService, SocialUser } from 'angular4-social-login';
import { FacebookLoginProvider } from 'angular4-social-login';
import { Router } from "@angular/router";
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Md5 } from 'ts-md5/dist/md5';
import { RequestOptions } from '@angular/http';
import { AppSettings } from '../reusable/app-settings';
import { PermissionService } from '../permission/permission.service';
@Injectable()
export class LoginService {

  private user: SocialUser;
  loggedIn: boolean;

  constructor(private permissionService : PermissionService, private httpClient: HttpClient, private authService: AuthService, private router: Router) {

  }




  signInWithFB() {
    console.log("login tried");
     this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(response => {
       console.log(response);
      localStorage.setItem('user', JSON.stringify(response));
      this.httpClient.post<Token>(AppSettings.FACEBOOK_LOGIN_ENDPOINT, response).subscribe(token=> {
        console.log(token);
        localStorage.setItem('access_token', token.token);
        this.permissionService.getUserGivenPermissions().then(permissions => {
          console.log(permissions); 
          localStorage.setItem('user_permissions',JSON.stringify(permissions));
        })
        //this.router.navigate(['app']);

      });
    });
  }


  signInWithCredentials(credentials) {
    let url     = AppSettings.GLOBAL_API_ENPOINT + 'login_check';
    let body     = new URLSearchParams();
    body.append('_username', credentials.email);
    body.append('_password', credentials.password);
  let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    let options ={ headers : headers};

  return this.httpClient
          .post<Token>(url, body.toString(), options);

  }

  signOut(): void {
    localStorage.removeItem('access_token');
    this.authService.signOut().then(response => {
        console.log(response);
    }).catch(error => {
      console.log('logged out response', error);
    });
    this.user = null;
  }


}


interface Token { // interface
  token: string;
}
