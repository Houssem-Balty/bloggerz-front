import {Component, Output, EventEmitter} from '@angular/core' ;
import {PageviewService} from '../pageview.service';
import { PermissionService } from '../../permission/permission.service';
import {NgForm, FormControl} from '@angular/forms';
import { Input } from '@angular/core/src/metadata/directives';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';

@Component({
    selector: 'app-pageviewfilter',
    templateUrl: './pageview-filter.component.html',
    styleUrls: ['./pageview-filter.component.css']
})
export class PageviewFilterComponent {
  private metrics;
  private period;

  private metricsList = ['page_impressions', 'page_engaged_users', 'page_consumptions'];
  private periods = ['day', 'week', 'days_28'];
  @Output() applyEventEmitter: EventEmitter<NgForm> = new EventEmitter<NgForm>();

  constructor( ) {
    this.metrics = 'page_impressions,page_views_total,page_engaged_users';
    this.period = 'day';

  }





  notifyOnSubmit(f: NgForm) {
    console.log(f.value);
    this.applyEventEmitter.emit(f) ;
  }

  onSubmit(f: NgForm) {
    this.notifyOnSubmit(f);
    this.metrics = f.value.metrics;
  }

  getMetrics() {
    return this.metrics ;
  }

}


