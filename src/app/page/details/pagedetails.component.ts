import { Component, Input } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute } from '@angular/router';
import { PageviewService } from '../pageview.service';
import { PermissionService } from '../../permission/permission.service';
import { ChartpageComponent } from './chart-page/chartpage.component';
import {PostinsightComponent} from '../details/post-insight/post-insight.component';
import {NgbModal, ModalDismissReasons,NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {MatSidenavModule} from '@angular/material/sidenav';
@Component({
  selector: 'app-pagedetails',
  templateUrl: './pagedetails.component.html',
})

export class PagedetailsComponent implements OnInit {
  @Input() id: any;
  @Input() type: any;
  rescheduleDate;
  rescheduleMessage;

  private token;
  private publicProfile;
  private latestPosts;
  closeResult;

  constructor(private modalService: NgbModal ,private route: ActivatedRoute, private pageviewService: PageviewService, private permissionService: PermissionService) {
    this.publicProfile = {};
    this.latestPosts = {};
    this.type = "photo";
  }

  ngOnInit() {
    this.route.paramMap
      .subscribe(params => {
        this.id = params.get('id');
      });
    this.permissionService.getPageAccessToken(this.id).then(accessToken => {
      this.token = accessToken;
      this.bindData(accessToken);



    }).catch(error => {
      console.log(error);
    });


    console.log(this.publicProfile);
  }




  bindData(token) {

    switch (this.type) {
      case 'photo':
        this.pageviewService.getPageLatestPhotos(this.id, this.token).then(posts => {
          console.log(posts);
          this.latestPosts = [posts.data];
        });
        break;
      case 'video':
        console.log('passing to video', this.latestPosts);
        this.pageviewService.getPageLatestVideos(this.id, this.token).then(posts => {
          console.log('videos = ', posts);
          this.latestPosts = [posts.data];
        });
        break;
      case 'link':
        console.log('passing to links', this.latestPosts);
        this.pageviewService.getPageLatestLinks(this.id, this.token).then(posts => {
          console.log('link = ', posts);
          this.latestPosts = [posts.data];
        });

    }
  }


  open($event,content, post) {
    $event.preventDefault();
    this.rescheduleDate = '';
    this.rescheduleMessage = '';
    this.modalService.open(content).result.then((result) => {
      console.log(result);
      if (result === 'validate'){
        this.reschedulePost(post,this.id); 
      }
    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }

  reschedulePost(post, id){
    this.permissionService.getUserPermissions('publish_actions,publish_pages').then( permission => {
      console.log(this.rescheduleDate.month+'/'+this.rescheduleDate.day+'/'+this.rescheduleDate.year);
     this.pageviewService.publishPost(id,post,this.token,this.rescheduleDate.month+'/'+this.rescheduleDate.day+'/'+this.rescheduleDate.year,this.rescheduleMessage).then(response => {
        console.log(response);
      });
      console.log(post);
    })
  }

  getDateAndReschedulePost(){
    console.log('clicked',this.rescheduleDate, this.rescheduleMessage);
  }
}
